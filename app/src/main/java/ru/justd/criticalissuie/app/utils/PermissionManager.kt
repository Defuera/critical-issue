package ru.justd.criticalissuie.app.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

fun processPermissionRequest(
        activity: Activity,
        permission: String,
        requestCode: Int,
        onExplainPermission: () -> Unit,
        onPermissionGranted: () -> Unit
) {

    if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

            onExplainPermission.invoke()

        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
        }

    } else {
        onPermissionGranted.invoke()
    }

}
