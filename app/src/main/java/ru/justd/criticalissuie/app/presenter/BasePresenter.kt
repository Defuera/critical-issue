package ru.justd.criticalissuie.app.presenter

import ru.justd.criticalissuie.app.view.BaseView
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action0
import rx.functions.Action1
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * Created by defuera on 19/09/2016.
 */
abstract class BasePresenter<V : BaseView> {

    lateinit var view: V

    val subscriptions = CompositeSubscription()

    fun attachView(view: V) {
        this.view = view

        onViewAttached()
    }

    fun detachView() {
        //todo view = null ?
        subscriptions.clear()
    }

    abstract fun onViewAttached()

    fun <T> subscribe(
            observable: Observable<T>,
            onNext: Action1<T> = Action1 { },
            onError: Action1<Throwable> = Action1 { },
            onCompleted: Action0 = Action0 { }
    ) {
        subscriptions.add(
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(onNext, onError, onCompleted)
        )
    }

}