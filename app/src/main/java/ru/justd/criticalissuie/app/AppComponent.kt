package ru.justd.criticalissuie.app

import dagger.Component
import ru.justd.criticalissuie.contacts.view.ContactsActivity
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(view: ContactsActivity)

}