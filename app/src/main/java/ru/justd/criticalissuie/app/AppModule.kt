package ru.justd.criticalissuie.app

import android.content.ContentResolver
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val context: Context) {

    @Singleton
    @Provides
    fun provideContentResolver(): ContentResolver {
        return context.contentResolver
    }
}