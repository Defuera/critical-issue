package ru.justd.criticalissuie.app

import android.app.Application

class CriticalIssueApp : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }
}
