package ru.justd.criticalissuie.app.utils

import android.database.Cursor

fun Cursor.getString(columnName: String) : String? {
    return getString(getColumnIndex(columnName))
}

fun Cursor.getLong(columnName: String) : Long? {
    return getLong(getColumnIndex(columnName))
}
