package ru.justd.criticalissuie.contacts.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import ru.justd.criticalissuie.R

class SplashActivity : AppCompatActivity() {

    private val DELAY_MILLIS = 1000L

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(
                {
                    startActivity(Intent(this, ContactsActivity::class.java))
                    finish()
                },
                DELAY_MILLIS
        )

    }

}
