package ru.justd.criticalissuie.contacts.view.widgets

import android.content.Context
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import ru.justd.criticalissuie.R
import ru.justd.criticalissuie.contacts.model.ContactViewModel

class ContactWidget : FrameLayout {

    lateinit var photo: ImageView
    lateinit var name: TextView
    lateinit var phone: TextView


    constructor(context: Context) : super(context) {
        init()
    }

    private fun init() {
        inflate(context, R.layout.widget_contact, this)
        //todo butterKnife

        name = findViewById(R.id.name) as TextView
        photo = findViewById(R.id.photo) as ImageView
    }

    fun bind(viewModel: ContactViewModel) {

        name.text = viewModel.name

        if (viewModel.photo != null) {
            Glide.with(context)
                    .load(viewModel.photo)
                    .into(photo)
        }
    }
}