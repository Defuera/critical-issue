package ru.justd.criticalissuie.contacts.model

import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract
import android.provider.ContactsContract.Contacts
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsService @Inject constructor() {

    @Inject
    lateinit var contentResolver: ContentResolver

    val projection = arrayOf(
            Contacts._ID,
            Contacts.DISPLAY_NAME,
            Contacts.PHOTO_THUMBNAIL_URI,
            Contacts.LOOKUP_KEY
    )

    fun loadContacts(): Observable<Cursor> {

        return Observable.just(
                contentResolver.query(
                        Contacts.CONTENT_URI,
                        projection,
                        null,
                        null,
                        ContactsContract.Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC")
        )

    }
}