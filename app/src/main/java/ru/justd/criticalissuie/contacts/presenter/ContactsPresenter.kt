package ru.justd.criticalissuie.contacts.presenter

import ru.justd.criticalissuie.app.presenter.BasePresenter
import ru.justd.criticalissuie.contacts.model.ContactsService
import ru.justd.criticalissuie.contacts.view.ContactsView
import rx.functions.Action1
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsPresenter @Inject constructor() : BasePresenter<ContactsView>() {

    @Inject
    lateinit var service: ContactsService

    override fun onViewAttached() {}

    fun loadData() {

        subscribe(
                service.loadContacts(),
                onNext = Action1 { cursor -> view.showData(cursor) },
                onError = Action1 { throwable -> view.showEmptyView() }
        )

    }

}