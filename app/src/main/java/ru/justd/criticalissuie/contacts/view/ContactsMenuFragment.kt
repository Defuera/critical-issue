package ru.justd.criticalissuie.contacts.view

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import ru.justd.criticalissuie.R
import ru.justd.criticalissuie.app.utils.processPermissionRequest
import ru.justd.criticalissuie.contacts.model.ContactViewModel

/**
 * * 4. По длинному тапу на отдельном элементе списка должно раскрываться меню (желательно BottomSheet) из следующих пунктов:
 * - Поделиться: шаринг интентом данных контакта во все поддерживаемые приложения
 * - Удалить: удаление контакта
 * - Отмена: закрытие меню
 */
class ContactsMenuFragment : BottomSheetDialogFragment() {

    companion object {

        private val EXTRA_CONTACT_VIEW_MODEL = "ru.justd.criticalissuie.extra_contact_view_model"

        fun show(fragmentManager: FragmentManager, contactViewModel: ContactViewModel) {

            val contactsMenuFragment = ContactsMenuFragment()
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_CONTACT_VIEW_MODEL, contactViewModel)

            contactsMenuFragment.arguments = bundle

            contactsMenuFragment
                    .show(
                            fragmentManager,
                            ContactsMenuFragment::class.java.simpleName
                    )
        }

    }

    lateinit var contactViewModel: ContactViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contacts_menu, container, false)

        contactViewModel = arguments.getSerializable(EXTRA_CONTACT_VIEW_MODEL) as ContactViewModel

        view.findViewById(R.id.share).setOnClickListener { share() }
        view.findViewById(R.id.remove).setOnClickListener { remove() }
        view.findViewById(R.id.cancel).setOnClickListener { dismiss() }
        return view
    }

    fun share() {
        dismiss()

        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, contactViewModel.name)
        sendIntent.type = "text/plain" //todo send contact data, not text
        startActivity(sendIntent)
    }

    fun remove() {

        processPermissionRequest(
                activity,
                Manifest.permission.WRITE_CONTACTS,
                ContactsActivity.MY_PERMISSIONS_REQUEST_WRITE_CONTACTS,
                { explainPermissionRequest() },
                { removeContact() }
        )
    }

    private fun removeContact() {

        val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contactViewModel.lookupKey)
        val deletedRows = activity.contentResolver.delete(uri, null, null)

        if (deletedRows > 0) {
            (activity as ContactsView).reloadData()
            dismiss()
        }

    }

    private fun explainPermissionRequest() {
        Toast.makeText(activity, "explainPermissionRequest", Toast.LENGTH_SHORT).show()
    }
}