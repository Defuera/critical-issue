package ru.justd.criticalissuie.contacts.view

import android.database.Cursor
import ru.justd.criticalissuie.app.view.BaseView

interface ContactsView : BaseView {
    fun showEmptyView()

    fun showData(cursor: Cursor)

    fun reloadData()
}