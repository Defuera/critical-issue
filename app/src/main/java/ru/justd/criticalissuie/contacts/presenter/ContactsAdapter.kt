package ru.justd.criticalissuie.contacts.presenter

import android.database.Cursor
import android.provider.ContactsContract.Contacts
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import ru.justd.criticalissuie.app.utils.*
import ru.justd.criticalissuie.contacts.model.ContactViewModel
import ru.justd.criticalissuie.contacts.view.widgets.ContactWidget

class ContactsAdapter(val cursor: Cursor, val longClickListener : (contactViewModel: ContactViewModel) -> Boolean) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ContactViewHolder(ContactWidget(parent.context))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        cursor.moveToPosition(position)

        val widget = (holder as ContactViewHolder).widget
        val contactId = cursor.getLong(Contacts._ID)
        val viewModel = ContactViewModel(
                contactId!!,
                cursor.getString(Contacts.DISPLAY_NAME)!!,
                cursor.getString(Contacts.PHOTO_THUMBNAIL_URI),
                cursor.getString(Contacts.LOOKUP_KEY)!!
        )
        widget.bind(
                viewModel)

        widget.setOnLongClickListener { longClickListener.invoke(viewModel) }
    }

    override fun getItemCount(): Int {
        return cursor.count
    }

    class ContactViewHolder(val widget: ContactWidget) : ViewHolder(widget)


}

