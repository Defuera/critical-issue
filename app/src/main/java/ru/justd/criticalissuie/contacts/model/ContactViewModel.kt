package ru.justd.criticalissuie.contacts.model

import java.io.Serializable

data class ContactViewModel(
        val id: Long,
        val name: String,
        val photo: String?,
        val lookupKey: String
) : Serializable