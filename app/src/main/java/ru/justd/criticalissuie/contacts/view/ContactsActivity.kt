package ru.justd.criticalissuie.contacts.view

import android.Manifest.permission.READ_CONTACTS
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.database.Cursor
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import ru.justd.criticalissuie.R
import ru.justd.criticalissuie.app.CriticalIssueApp
import ru.justd.criticalissuie.app.utils.processPermissionRequest
import ru.justd.criticalissuie.contacts.model.ContactViewModel
import ru.justd.criticalissuie.contacts.presenter.ContactsAdapter
import ru.justd.criticalissuie.contacts.presenter.ContactsPresenter
import javax.inject.Inject

/**
 * 1. Приложение должно отображать форматированный список контактов телефона v
 * 2. Приложение должно поддерживать переворот экрана v
 * 3. В приложение должен быть сплеш-экран v
 * 4. По длинному тапу на отдельном элементе списка должно раскрываться меню (желательно BottomSheet) из следующих пунктов:
 * - Поделиться: шаринг интентом данных контакта во все поддерживаемые приложения
 * - Удалить: удаление контакта
 * - Отмена: закрытие меню
 * 5. В приложение должна быть обработка запросов разрешений для Android 6+ v
 *
 *
 * Дополнительно:
 * 1. Разбить список на секции по первой букве имени контакта
 * 2. Добавить сбоку меню быстрого скролла по буквам (см. Google Contacts для примера)
 */
class ContactsActivity : AppCompatActivity(), ContactsView {

    companion object {
        val MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0
        val MY_PERMISSIONS_REQUEST_WRITE_CONTACTS = 1
    }

    lateinit var recycler: RecyclerView
    lateinit var noDataView: View

    @Inject lateinit var presenter: ContactsPresenter

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CriticalIssueApp.component.inject(this)
        setContentView(R.layout.activity_main)

        recycler = findViewById(R.id.recycler) as RecyclerView
        noDataView = findViewById(R.id.no_data)

        recycler.layoutManager = LinearLayoutManager(this)

        checkPermission()
    }

    private fun checkPermission() {
        processPermissionRequest(
                this,
                READ_CONTACTS,
                MY_PERMISSIONS_REQUEST_READ_CONTACTS,
                { explainPermissionRequest() },
                { presenter.loadData() }
        )
    }

    private fun explainPermissionRequest() {
        AlertDialog
                .Builder(this)
                .setTitle(R.string.permission_request_contacts_title)
                .setMessage(R.string.permission_request_contacts)
                .setPositiveButton(R.string.ok, {
                    dialogInterface, i ->
                    ActivityCompat.requestPermissions(
                            this,
                            arrayOf(READ_CONTACTS),
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS)
                })
                .setNegativeButton(R.string.cancel, { d, i -> finish() })
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_CONTACTS -> {

                if (grantResults.size > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    presenter.loadData()

                } else {
                    explainPermissionRequest()
                }

            }
        }
    }


    override fun showEmptyView() {
        noDataView.visibility = View.VISIBLE
        recycler.visibility = View.GONE
    }

    override fun showData(cursor: Cursor) {
        noDataView.visibility = View.GONE
        recycler.visibility = View.VISIBLE

        val contactsAdapter = ContactsAdapter(cursor) {
            contactViewModel ->
            displayBottomSheetForContact(contactViewModel)
        }

        recycler.adapter = contactsAdapter
    }

    override fun reloadData() {
        presenter.loadData()
    }

    private fun displayBottomSheetForContact(contactViewModel: ContactViewModel): Boolean {

        ContactsMenuFragment.show(supportFragmentManager, contactViewModel)

        return true
    }

}
